// console.log("Testing");

// What are conditional statements?
// Conditional statements allow us to control the flow of our program and it allows us to run statement/instruction based on the condition.

// if statement
// Executes a statement fi a specified condition is true

/*
	Syntax:
	if (condition) {
		code block / statement
	}
*/

let numA = 0;

if (numA < 0) {
	console.log("Hello");
}

// The codeblock inside the if statement will not work if the value of the condition isn't true


if (numA == 0) {
	console.log("Hello");
}


console.log(numA < 0);
console.log(numA == 0);

let city = "New York";

if (city == "New York") {
	console.log("Welcome to New York City");
}


// else if clause

/*
	- Executes a statement if previous condition/s are false and if the specified condition is true
	- The "else if" clause is optional and can be added to capture additional conditions to change the flow of the program
*/


let numH = 0;

if (numH < 0) { //returns to false 
	console.log("Hello");
} else if (numH > 0) { // returns to false
	consolelog("World");
} else if (numH == 0) {
	console.log("The value is zero");
}

// Alternative code.

if (numH < 0) { //returns to false 
	console.log("The number is negative");
} else if (numH > 0) { // returns to false
	consolelog("The number is positive");
} else {
	console.log("The value is zero");
}

// else statement

/*
	- Executes a statement if all other conditions are false
	- The 'else' statement is optional and can be added to capture any other result to change the flow of the program.
*/

// city = "New York";

if (city === "New York") {
	console.log("Welcome to New York City");
} else if (city === "Tokyo") {
	console.log("Welcome to Tokyo, Japan");
} else {
	console.log("City is not included in the list.");
}

let username = "admin";
let password = "admin1234";

if (username === "admin") {
	console.log("Correct username");
}

if (password == "admin1234") {
	console.log("Correct password");
} else {
	console.log("Wrong username or password");
}

if (username == "admin" && password == "admin1234") {
	console.log("Successful login");
} else {
	console.log("Wrong username or password");
}


let message = "No message";
	console.log(message);

	function determineTyphooneIntensity(windSpeed){

		if(windSpeed < 30){
			return "Not a typhoon yet."
		}
		else if(windSpeed <= 61){
			return "Tropical depression detected."
		}
		else if (windSpeed >= 62 && windSpeed <= 88){
			return "Tropical storm detected."
		}
		else if(windSpeed >= 89 && windSpeed <= 117){
			return "Severe Tropical storm deteceted";
		}
		else{
			return "Typhoon detected."
		}
	}

	message = determineTyphooneIntensity(110);
	console.log(message);

	//  console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code.
	if(message === "Severe Tropical storm deteceted"){
		console.warn(message);
	}

// [SECTION] Truthy and Falsy
/*
	- In javascript a "truthy" value that is considered true when encountered in a boolean context.
	- False
		1. false
		2. 0
		3. ""
		4. null
		5. undefined // let number;
		6. NaN (Not a Number)
*/


let isMarried = true;
// Truthy exmaples:

if (true) {
	console.log("Truthy");
}

if (1) {
	console.log("Truthy");
}

if ([]) {
	console.log("Truthy");
}


// Falsy Examples

if (false) {
	console.log("Falsy");
}

if (0) {
	console.log('Falsy');
}

if (undefined) {
	console.log("Falsy");
}

if (isMarried) {
	console.log("Truthy");
}

// [SECTION] Conditional (Ternary) Operator

/*

	- The conditional (Ternary) operator takes in three operands

*/


let ternaryResult = (1 < 18) ? true : false;

console.log("Result of Ternary Operator " + ternaryResult);


// Multiple statement execution using ternary operator

let name;

function isOfLegalAge() {
	name = "John";
	return "Your are in legal age";
}

function underAge() {
	name = "Jane";
	return "You are under age limit";
}

// parseInt - converts the input into an integer
// 

/*let age = parseInt(prompt("What is your age?"));

console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : underAge();

console.log("Return of Ternary Operator in functions " + legalAge + ", " + name);
*/

// [SECTION] Switch Statements

// let day = prompt("What day of the week is it today?");

/*day = day.toUpperCase();

console.log(day);*/

/*switch(day) {
	case "MONDAY":
		console.log("The color of the day is Red");
		break;
	case "TUESDAY":
		console.log("The color of the day is Orange");
		break;
	case "WEDNESDAY":
		console.log("The color of the day is Yellow");
		break;
	case "THURSDAY":
		console.log("The color of the day is green");
		break;
	case "FRIDAY":
		console.log("The color of the day is blue");
		break;
	case "SATURDAY":
		console.log("The color of the day is indigo");
		break;
	case "SUNDAY":
		console.log("The color of the day is violet");
		break;
	default:
		console.log("That wasn't a day!");

}*/

// [SECTION] Try-Catch-Finally

let codeErrorMessage = "Code Error";
							// parameter
function showIntensityAlert(windSpeed) {
	try {
		// codes / code block to try
		alerat(determineTyphooneIntensity(windSpeed));
	} 

	catch(error) {
		// error.message is used to access the information relating to an error object

		console.warn(error.message);
	}

	finally {
		alert("Intensity udpates will show new alert.")
	}

}

showIntensityAlert(56);

console.log("Sample Output after try-catch-finally statement");



